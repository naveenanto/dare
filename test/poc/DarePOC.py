import json
import random

from dare.DareInterface import DareInterface

key = 'zeGPWatgeyk8WWSZZGwj8g'
with open('../samples/sample_template.json') as template_file:
    data = json.load(template_file)

if DareInterface.get_embedding(key) is None:
    DareInterface.create_embedder_from_config(key=key, config=data)

user_ids_set = set([random.randint(1, 6) for i in range(3)])
user_ids = [user for user in user_ids_set]

with open('../samples/sample_user.json') as user_file:
    user_data = json.load(user_file)

print("Before Training")
print(DareInterface.predict(key, user_ids=user_ids, items=user_data))

likes = [1, 0, 0, 1]
for i in range(len(likes)):
    DareInterface.train(key, user_ids=user_ids, items=[user_data[i]], score=[likes[i]])
#
print("After Training")
print(DareInterface.predict(key, user_ids=user_ids, items=user_data))
