import unittest

from lib.auto_embedder.AsIsHandler import AsIsHandler
from lib.auto_embedder.CategoryHandler import CategoryHandler
from lib.auto_embedder.Embedder import Embedder
from lib.auto_embedder.LinearHandler import LinearHandler


class TestTemplateParser(unittest.TestCase):
    def test_asis_embedding_basic(self):
        template = {'a': '$asis'}
        tp = Embedder(template)
        self.assertTrue(isinstance(tp.embedding_mapper['a'], AsIsHandler))

    def test_asis_embedding_multilevel(self):
        template = {'a': {'b': '$asis'}}
        tp = Embedder(template)
        self.assertTrue(isinstance(tp.embedding_mapper['a']['b'], AsIsHandler))

    def test_linear_embedding_min_max(self):
        template = {'a': {"$linear": {"$min": 4, "$max": 7}}}
        tp = Embedder(template)
        self.assertTrue(isinstance(tp.embedding_mapper['a'], LinearHandler))
        self.assertEqual(tp.embedding_mapper['a'].min_max[0], 4)
        self.assertEqual(tp.embedding_mapper['a'].min_max[1], 7)

    def test_linear_embedding_min_max_multilevel(self):
        template = {'a': {'b': {"$linear": {"$min": 4, "$max": 7}}}}
        tp = Embedder(template)
        self.assertTrue(isinstance(tp.embedding_mapper['a']['b'], LinearHandler))
        self.assertEqual(tp.embedding_mapper['a']['b'].min_max[0], 4)
        self.assertEqual(tp.embedding_mapper['a']['b'].min_max[1], 7)

    def test_linear_embedding_mean_std(self):
        template = {'a': {"$linear": {"$mean": 10, "$std": 0.1}}}
        tp = Embedder(template)
        self.assertTrue(isinstance(tp.embedding_mapper['a'], LinearHandler))
        self.assertEqual(tp.embedding_mapper['a'].mean_std[0], 10)
        self.assertEqual(tp.embedding_mapper['a'].mean_std[1], 0.1)

    def test_linear_embedding_mean_std_multilevel(self):
        template = {'a': {'b': {"$linear": {"$mean": 10, "$std": 0.1}}}}
        tp = Embedder(template)
        self.assertTrue(isinstance(tp.embedding_mapper['a']['b'], LinearHandler))
        self.assertEqual(tp.embedding_mapper['a']['b'].mean_std[0], 10)
        self.assertEqual(tp.embedding_mapper['a']['b'].mean_std[1], 0.1)

    def test_category_set_basic_empty(self):
        template = {"a": {"$category": {"$set": []}}}
        tp = Embedder(template)
        self.assertTrue(isinstance(tp.embedding_mapper['a'], CategoryHandler))
        self.assertEqual(tp.embedding_mapper['a'].last_index, 0)

    def test_category_set_basic_filled(self):
        template = {"a": {"$category": {"$set": ["1", "2", "3"]}}}
        tp = Embedder(template)
        self.assertTrue(isinstance(tp.embedding_mapper['a'], CategoryHandler))
        self.assertTrue("1" in tp.embedding_mapper['a'].index_map)
        self.assertEqual(tp.embedding_mapper['a'].last_index, 3)

    def test_category_set_get_keys(self):
        set_values = ["1", "2"]
        template = {"a": {"$category": {"$set": set_values}}}
        tp = Embedder(template)
        self.assertTrue(isinstance(tp.embedding_mapper['a'], CategoryHandler))
        sorted_keys = sorted(list(tp.embedding_mapper['a'].get_keys()))
        self.assertListEqual(set_values, sorted_keys)

    def test_category_range_basic(self):
        template = {"a": {"$category": {"$range": [0, 7]}}}
        tp = Embedder(template)
        self.assertTrue(isinstance(tp.embedding_mapper['a'], CategoryHandler))
        self.assertTrue(7 in tp.embedding_mapper['a'].index_map)
        self.assertEqual(tp.embedding_mapper['a'].last_index, 8)

    def test_category_group_range_basic(self):
        template = {"a": {"$category": {"$group_range": [[1000, 2000], [2001, 3000], [3001, 4000]]}}}
        tp = Embedder(template)
        self.assertTrue(isinstance(tp.embedding_mapper['a'], CategoryHandler))
        self.assertEqual(tp.embedding_mapper['a'].last_index, 4)

    def test_template_insertion_shallow(self):
        template = {"a": {"$category": {"$set": ["1", "2", "3"]}}}
        tp = Embedder(template)
        tp.add_to_template({"b": {"$category": {"$set": [5, 6, 7]}}})
        self.assertTrue(isinstance(tp.embedding_mapper['a'], CategoryHandler))
        self.assertTrue(isinstance(tp.embedding_mapper['b'], CategoryHandler))
        self.assertTrue("1" in tp.embedding_mapper['a'].index_map)
        self.assertTrue(5 in tp.embedding_mapper['b'].index_map)
        # self.assertEqual(tp.embedding_mapper['a'].last_index, 3)

    def test_template_insertion_multiple(self):
        template = {"a": {"$category": {"$set": ["1", "2", "3"]}}}
        tp = Embedder(template)
        tp.add_to_template({"b": {"$category": {"$set": [5, 6, 7]}}, "c": "$asis"})
        self.assertTrue(isinstance(tp.embedding_mapper['a'], CategoryHandler))
        self.assertTrue(isinstance(tp.embedding_mapper['b'], CategoryHandler))
        self.assertTrue(isinstance(tp.embedding_mapper['c'], AsIsHandler))

    def test_template_insertion_deep(self):
        template = {"a": {"b": {"$category": {"$set": ["1", "2", "3"]}}}}
        tp = Embedder(template)
        tp.add_to_template({"a": {"c": {"$category": {"$set": [5, 6, 7]}}}})
        self.assertTrue(isinstance(tp.embedding_mapper['a']['b'], CategoryHandler))
        self.assertTrue(isinstance(tp.embedding_mapper['a']['c'], CategoryHandler))
        self.assertTrue("1" in tp.embedding_mapper['a']['b'].index_map)
        self.assertTrue(5 in tp.embedding_mapper['a']['c'].index_map)

    def test_template_delete_category(self):
        template = {"a": {"b": {"$category": {"$set": ["1", "2", "3"]}}},
                    "x": {"$linear": {"$mean": 10, "$std": 0.1}},
                    "y": "$asis"}
        tp = Embedder(template)
        tp.add_to_template({"c": {"$category": {"$set": [5, 6, 7]}}})
        last_index_before_deletion = tp.last_index
        transfer_map = tp.delete_key_from_embedding_map('a')
        last_index_after_deletion = tp.last_index
        self.assertNotEqual(last_index_before_deletion, last_index_after_deletion)
        self.assertTrue('a' not in tp.embedding_mapper)

    def test_template_delete_linear(self):
        template = {"a": {"b": {"$category": {"$set": ["1", "2", "3"]}}},
                    "x": {"$linear": {"$mean": 10, "$std": 0.1}},
                    "y": "$asis"}
        tp = Embedder(template)
        last_index_before_deletion = tp.last_index
        transfer_map = tp.delete_key_from_embedding_map('x')
        last_index_after_deletion = tp.last_index
        self.assertNotEqual(last_index_before_deletion, last_index_after_deletion)
        self.assertTrue('x' not in tp.embedding_mapper)


if __name__ == 'main':
    unittest.main()
