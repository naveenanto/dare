import unittest

import numpy as np

from lib.auto_embedder.Embedder import Embedder


class TestEmbedder(unittest.TestCase):
    def test_single_input_asis_embedder_basic(self):
        template = {'a': '$asis'}
        data = {'a': 0.85}
        tp = Embedder(template)
        embedding = tp.embed(data)
        self.assertEqual(embedding[0], 0.85)

    def test_unknown_key_in_data(self):
        template = {'a': '$asis'}
        data = {'a': 0.85, 'b': 92}
        tp = Embedder(template)
        embedding = tp.embed(data)
        self.assertEqual(len(embedding), 1)

    def test_single_input_asis_embedder_multilevel(self):
        template = {'a': {'b': '$asis', 'c': '$asis'}}
        data = {'a': {'b': 0.85, 'c': 0.75}}
        tp = Embedder(template)
        embedding = tp.embed(data)
        self.assertEqual(embedding[0], 0.85)
        self.assertEqual(embedding[1], 0.75)

    def test_single_input_linear_embedder_min_max(self):
        template = {'a': {"$linear": {"$min": 4, "$max": 7}}}
        data = {'a': 5}
        tp = Embedder(template)
        embedding = tp.embed(data)
        self.assertAlmostEqual(embedding[0], -0.166, delta=0.001)

    def test_single_input_linear_embedder_mean_std(self):
        template = {'a': {"$linear": {"$mean": 4, "$std": 7}}}
        data = {'a': 5}
        tp = Embedder(template)
        embedding = tp.embed(data)
        self.assertAlmostEqual(embedding[0], 0.1428, delta=0.001)

    def test_single_input_category_set_basic_empty(self):
        template = {"a": {"$category": {"$set": []}}}
        data = {'a': 'elem_1'}
        tp = Embedder(template)
        embedding = tp.embed(data)
        self.assertEqual(embedding[0], 1.0)

    def test_multi_input_category_set_basic_empty_dynamic_pos(self):
        template = {"a": {"$category": {"$set": []}}}
        data_1 = {'a': 'elem_1'}
        data_2 = {'a': 'elem_3'}
        tp = Embedder(template)
        embedding_1 = tp.embed(data_1)
        embedding_2 = tp.embed(data_2)
        self.assertGreater(len(embedding_2), len(embedding_1))
        padded_embedding_1 = np.array(tp.get_updated_embedding(embedding_1))
        self.assertFalse(np.array_equal(embedding_2, padded_embedding_1))

    def test_single_input_category_set_basic_filled(self):
        template = {"a": {"$category": {"$set": ["elem_1", "elem_2"]}}}
        data = {'a': 'elem_1'}
        tp = Embedder(template)
        embedding = tp.embed(data)
        self.assertEqual(len(embedding), 2)
        self.assertTrue(embedding[0] == 1.0 or embedding[1] == 1.0)

    def test_multi_input_category_set_basic_filled_exact_pos(self):
        template = {"a": {"$category": {"$set": ["elem_1", "elem_2"]}}}
        data_1 = {'a': 'elem_1'}
        data_2 = {'a': 'elem_1'}
        tp = Embedder(template)
        embedding_1 = tp.embed(data_1)
        embedding_2 = tp.embed(data_2)
        self.assertEqual(embedding_1, embedding_2)

    def test_multi_input_category_set_basic_filled_different_pos(self):
        template = {"a": {"$category": {"$set": ["elem_1", "elem_2"]}}}
        data_1 = {'a': 'elem_1'}
        data_2 = {'a': 'elem_2'}
        tp = Embedder(template)
        embedding_1 = tp.embed(data_1)
        embedding_2 = tp.embed(data_2)
        self.assertNotEqual(embedding_1, embedding_2)

    def test_multi_input_category_set_basic_filled_dynamic_pos(self):
        template = {"a": {"$category": {"$set": ["elem_1", "elem_2"]}}}
        data_1 = {'a': 'elem_1'}
        data_2 = {'a': 'elem_3'}
        tp = Embedder(template)
        embedding_1 = tp.embed(data_1)
        embedding_2 = tp.embed(data_2)
        self.assertGreater(len(embedding_2), len(embedding_1))
        padded_embedding_1 = np.array(tp.get_updated_embedding(embedding_1))
        self.assertFalse(np.array_equal(embedding_2, padded_embedding_1))

    def test_updated_emb(self):
        template = {"a": {"$category": {"$set": ["elem_1", "elem_2"]}}}
        data_1 = {'a': 'elem_1'}
        data_2 = {'a': 'elem_3'}
        tp = Embedder(template)
        embedding_1 = tp.embed(data_1)
        embedding_2 = tp.embed(data_2)
        self.assertEqual(len(tp.get_updated_embedding(embedding_2)), len(tp.get_updated_embedding(embedding_1)))

    def test_single_input_category_range_basic(self):
        template = {"a": {"$category": {"$range": [0, 7]}}}
        data = {'a': 2}
        tp = Embedder(template)
        embedding = tp.embed(data)
        self.assertEqual(len(embedding), 8)
        self.assertIn(1.0, embedding)

    def test_multi_input_category_range_exact_pos(self):
        template = {"a": {"$category": {"$range": [0, 7]}}}
        data_1 = {'a': 2}
        data_2 = {'a': 2}
        tp = Embedder(template)
        embedding_1 = tp.embed(data_1)
        embedding_2 = tp.embed(data_2)
        self.assertEqual(embedding_2, embedding_1)

    def test_multi_input_category_range_different_pos(self):
        template = {"a": {"$category": {"$range": [0, 7]}}}
        data_1 = {'a': 2}
        data_2 = {'a': 5}
        tp = Embedder(template)
        embedding_1 = tp.embed(data_1)
        embedding_2 = tp.embed(data_2)
        self.assertNotEqual(embedding_2, embedding_1)

    def test_single_input_category_group_range_basic(self):
        template = {"a": {"$category": {"$group_range": [[1000, 2000], [2001, 3000], [3001, 4000]]}}}
        data = {'a': 1500}
        tp = Embedder(template)
        embedding = tp.embed(data)
        self.assertIn(1.0, embedding)

    def test_single_input_category_group_range_outside(self):
        template = {"a": {"$category": {"$group_range": [[1000, 2000], [2001, 3000], [3001, 4000]]}}}
        data = {'a': 6500}
        tp = Embedder(template)
        embedding = tp.embed(data)
        self.assertEqual(1.0, embedding[-1])

    def test_multi_input_category_group_range_exact_pos(self):
        template = {"a": {"$category": {"$group_range": [[1000, 2000], [2001, 3000], [3001, 4000]]}}}
        data_1 = {'a': 1500}
        data_2 = {'a': 1700}
        tp = Embedder(template)
        embedding_1 = tp.embed(data_1)
        embedding_2 = tp.embed(data_2)
        self.assertEqual(embedding_1, embedding_2)

    def test_multi_input_category_group_range_different_pos(self):
        template = {"a": {"$category": {"$group_range": [[1000, 2000], [2001, 3000], [3001, 4000]]}}}
        data_1 = {'a': 1500}
        data_2 = {'a': 2700}
        tp = Embedder(template)
        embedding_1 = tp.embed(data_1)
        embedding_2 = tp.embed(data_2)
        self.assertNotEqual(embedding_1, embedding_2)

    def test_delete_transfer_map_index_addition(self):
        template = {"a": {"b": {"$category": {"$set": ["1", "2", "3"]}}},
                    "x": {"$linear": {"$mean": 10, "$std": 0.1}},
                    "y": "$asis"}
        tp = Embedder(template)
        tp.add_to_template({"c": {"$category": {"$set": [5, 6, 7]}}})
        data_2 = {'c': 9}
        last_index_before_deletion = tp.last_index
        tp.embed(data_2)
        transfer_map = tp.delete_key_from_embedding_map('a')
        last_index_after_deletion = tp.last_index
        self.assertNotEqual(last_index_before_deletion, last_index_after_deletion)
        self.assertTrue('a' not in tp.embedding_mapper)


if __name__ == 'main':
    unittest.main()
