import unittest

import numpy as np

from lib.auto_embedder.Embedder import Embedder
from lib.core.ItemUtility import ItemUtility
from lib.core.MathLab import MathLab


class TestItemEmbedder(unittest.TestCase):
    def test_object_creation_from_item_embedding(self):
        template = {'a': '$asis', 'b': '$asis'}
        tp = Embedder(template)
        item = ItemUtility(tp, embedding=[0.85])
        self.assertTrue(np.array_equal([0.85, 0.0], item.get_embedding()))

    def test_object_creation_from_item_data(self):
        template = {'a': '$asis'}
        data = {'a': 0.85}
        tp = Embedder(template)
        item = ItemUtility(tp, item=data)
        self.assertTrue(np.array_equal([0.85], item.get_embedding()))

    def test_math_lab_cosine_similarity_single_sample_single_feature(self):
        item_1 = np.array([0.85])
        item_2 = np.array([0.85])
        self.assertAlmostEqual(MathLab.cosine_similarity(item_1, item_2), 1.0)

    def test_math_lab_cosine_similarity_single_sample_multi_feature(self):
        item_1 = np.array([0.85, 0.6])
        item_2 = np.array([0.85, 0.6])
        self.assertAlmostEqual(MathLab.cosine_similarity(item_1, item_2), 1.0)

    def test_item_similarity_simple_equal(self):
        template = {'a': '$asis'}
        data = {'a': 0.85}
        tp = Embedder(template)
        item = ItemUtility(tp, item=data)
        self.assertTrue(item.get_similarity(other_items=[data]) == 1.0)

    def test_item_similarity_multiple_features_equal(self):
        template = {'a': '$asis', 'b': '$asis'}
        data = {'a': 0.85, 'b': 0.1}
        tp = Embedder(template)
        item = ItemUtility(tp, item=data)
        result = item.get_similarity(other_items=[data])
        self.assertAlmostEqual(result[0][0], 1.0)

    def test_item_similarity_multiple_features_different_item(self):
        template = {'a': '$asis', 'b': '$asis'}
        data = {'a': 0.85, 'b': 0.1}
        tp = Embedder(template)
        item = ItemUtility(tp, item=data)
        data_2 = {'a': 0.1, 'b': 0.85}
        result = item.get_similarity(other_items=[data_2])
        self.assertAlmostEqual(result[0][0], 0.23, delta=0.009)

    def test_item_similarity_multiple_features_different_item(self):
        template = {'a': '$asis', 'b': '$asis'}
        data = {'a': 0.85, 'b': 0.1}
        tp = Embedder(template)
        item = ItemUtility(tp, item=data)
        data_2 = {'a': 0.1, 'b': 0.85}
        result = item.get_similarity(other_items=[data_2])
        self.assertAlmostEqual(result[0][0], 0.23, delta=0.009)

    def test_item_similar_items_sorted_multiple_features_different_items(self):
        template = {'a': '$asis', 'b': '$asis'}
        data = {'a': 0.85, 'b': 0.1}
        tp = Embedder(template)
        item = ItemUtility(tp, item=data)
        data_2 = [{'id': 1, 'a': 0.1, 'b': 0.85}, {'id': 2, 'a': 0.9, 'b': 0.2}, {'id': 3, 'a': 0.6, 'b': 0.85}]
        scores, ids = item.get_similar_items_sorted(other_items=data_2)
        self.assertEqual(len(ids), len(data_2))
        self.assertEqual(data_2[ids[0]]['id'], 2)
        self.assertAlmostEqual(scores[ids[0]], 0.994, delta=0.001)

    def test_item_similar_items_sorted_multiple_features_different_items_limit(self):
        template = {'a': '$asis', 'b': '$asis'}
        data = {'a': 0.85, 'b': 0.1}
        tp = Embedder(template)
        item = ItemUtility(tp, item=data)
        data_2 = [{'id': 1, 'a': 0.1, 'b': 0.85}, {'id': 2, 'a': 0.9, 'b': 0.2}, {'id': 3, 'a': 0.6, 'b': 0.85}]
        scores, ids = item.get_similar_items_sorted(other_items=data_2, limit=2)
        self.assertEqual(len(ids), 2)
        self.assertEqual(data_2[ids[0]]['id'], 2)
        self.assertAlmostEqual(scores[ids[0]], 0.994, delta=0.001)

    def test_item_similarity_multiple_features_different_embedding(self):
        template = {'a': '$asis', 'b': '$asis'}
        data = {'a': 0.85, 'b': 0.1}
        tp = Embedder(template)
        item = ItemUtility(tp, item=data)
        data_2 = {'a': 0.1, 'b': 0.85}
        embedding = tp.embed(data_2)
        result = item.get_similarity(other_items=[embedding])
        self.assertAlmostEqual(result[0][0], 0.23, delta=0.009)

    def test_item_similarity_multiple_features_different_item_utility(self):
        template = {'a': '$asis', 'b': '$asis'}
        data = {'a': 0.85, 'b': 0.1}
        tp = Embedder(template)
        item = ItemUtility(tp, item=data)
        data_2 = {'a': 0.1, 'b': 0.85}
        item_2 = ItemUtility(tp, item=data_2)
        result = item.get_similarity(other_items=[item_2])
        self.assertAlmostEqual(result[0][0], 0.23, delta=0.009)

    def test_similarity_multiple_features_multiple_sample(self):
        items_for_comparison = np.array([[0.85, 0.6], [0.9, 0.2]])
        items_to_find_similarity = np.array([[0.85, 0.6], [0.85, 0.6]])
        result = MathLab.cosine_similarity(items_for_comparison, items_to_find_similarity)
        self.assertAlmostEqual(result[0][0], 1.0)
        self.assertAlmostEqual(result[0][1], 1.0)
        self.assertGreater(1.0, result[1][0])
