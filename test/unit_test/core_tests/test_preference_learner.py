import unittest

from lib.core.PreferenceLearner import PreferenceLearner


class TestPreferenceLearner(unittest.TestCase):
    def setUp(self):
        self.preference_learner = PreferenceLearner()

    def test_single_sample_single_feature_default_preference_prediction(self):
        item_embedding = [0.85]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(item_embedding))
        score = self.preference_learner.predict(preference_embedding, item_embedding)
        self.assertAlmostEqual(score[0][0], 0.50, delta=0.01)

    def test_single_sample_single_feature_multi_preference_prediction(self):
        item_embedding = [0.85]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embedding)),
                                 self.preference_learner.create_default_preference_embedding(len(item_embedding))]
        score = self.preference_learner.predict(preference_embeddings, item_embedding)
        self.assertAlmostEqual(score[0][0], score[1][0], delta=0.01)

    def test_single_sample_multiple_feature_default_preference_prediction(self):
        item_embedding = [0.85, 1.0, 0.0, 0.0, 1.0]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(item_embedding))
        score = self.preference_learner.predict(preference_embeddings=preference_embedding,
                                                item_embeddings=item_embedding)

        self.assertAlmostEqual(score[0][0], 0.50, delta=0.01)

    def test_single_sample_multiple_feature_multi_preference_prediction(self):
        item_embedding = [0.85, 1.0, 0.0, 0.0, 1.0]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embedding)),
                                 self.preference_learner.create_default_preference_embedding(len(item_embedding))]
        score = self.preference_learner.predict(preference_embeddings=preference_embeddings,
                                                item_embeddings=item_embedding)

        self.assertAlmostEqual(score[0][0], score[1][0], delta=0.01)

    def test_multiple_sample_single_feature_default_preference_prediction(self):
        item_embeddings = [[0.85], [0.65], [0.1], [1.0]]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(item_embeddings[0]))
        score = self.preference_learner.predict(preference_embedding, item_embeddings)
        self.assertAlmostEqual(score[0][0], 0.50, delta=0.01)
        self.assertAlmostEqual(score[0][3], 0.50, delta=0.01)

    def test_multiple_sample_single_feature_multi_preference_prediction(self):
        item_embeddings = [[0.85], [0.65], [0.1], [1.0]]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embeddings[0])),
                                 self.preference_learner.create_default_preference_embedding(len(item_embeddings[0]))]
        score = self.preference_learner.predict(preference_embeddings, item_embeddings)
        self.assertAlmostEqual(score[0][0], score[1][0], delta=0.01)
        self.assertAlmostEqual(score[0][3], score[1][3], delta=0.01)

    def test_multiple_sample_multiple_feature_default_preference_prediction(self):
        item_embeddings = [[0.85, 1.0, 0.0, 0.0, 1.0], [0.25, 0.0, 0.0, 0.0, 1.0]]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(item_embeddings[0]))
        score = self.preference_learner.predict(preference_embedding, item_embeddings)
        self.assertGreater(score[0][0], score[0][1])

    def test_multiple_sample_multiple_feature_multi_preference_prediction(self):
        item_embeddings = [[0.85, 1.0, 0.0, 0.0, 1.0], [0.25, 0.0, 0.0, 0.0, 1.0]]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embeddings[0])),
                                 self.preference_learner.create_default_preference_embedding(len(item_embeddings[0]))]
        score = self.preference_learner.predict(preference_embeddings, item_embeddings)
        self.assertGreater(score[0][0], score[0][1])
        self.assertGreater(score[1][0], score[1][1])

    def test_single_sample_single_feature_train_pos_no_repeat(self):
        item_embedding = [0.85]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(item_embedding))
        score_before_training = self.preference_learner.predict(preference_embedding, item_embedding)
        preference_embedding = self.preference_learner.train(preference_embedding, item_embedding, 1.0)
        score_after_training = self.preference_learner.predict(preference_embedding, item_embedding)
        self.assertGreater(score_after_training[0][0], score_before_training[0][0])

    def test_multiple_sample_single_feature_train_pos_no_repeat(self):
        item_embeddings = [[0.85], [0.1]]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(item_embeddings[0]))
        score_before_training = self.preference_learner.predict(preference_embedding, item_embeddings)
        preference_embedding = self.preference_learner.train(preference_embedding, item_embeddings, 1.0)
        score_after_training = self.preference_learner.predict(preference_embedding, item_embeddings)
        self.assertGreater(score_after_training[0][0], score_before_training[0][0])
        self.assertGreater(score_after_training[0][1], score_before_training[0][1])

    def test_single_sample_single_feature_multi_pref_train_pos_no_repeat(self):
        item_embedding = [0.85]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embedding)),
                                 self.preference_learner.create_default_preference_embedding(len(item_embedding))]
        score_before_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        preference_embeddings = self.preference_learner.train(preference_embeddings, item_embedding, 1.0)
        score_after_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        self.assertGreater(score_after_training[0][0], score_before_training[0][0])
        self.assertGreater(score_after_training[1][0], score_before_training[1][0])

    def test_multi_sample_single_feature_multi_pref_train_pos_no_repeat(self):
        item_embeddings = [[0.85], [0.1]]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embeddings[0])),
                                 self.preference_learner.create_default_preference_embedding(len(item_embeddings[0]))]
        score_before_training = self.preference_learner.predict(preference_embeddings, item_embeddings)
        preference_embeddings = self.preference_learner.train(preference_embeddings, item_embeddings, 1.0)
        score_after_training = self.preference_learner.predict(preference_embeddings, item_embeddings)
        self.assertGreater(score_after_training[0][0], score_before_training[0][0])
        self.assertGreater(score_after_training[0][1], score_before_training[0][1])
        self.assertGreater(score_after_training[1][0], score_before_training[1][0])
        self.assertGreater(score_after_training[1][1], score_before_training[1][1])

    def test_single_sample_single_feature_train_neg_no_repeat(self):
        item_embedding = [0.85]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(item_embedding))
        score_before_training = self.preference_learner.predict(preference_embedding, item_embedding)
        preference_embedding = self.preference_learner.train(preference_embedding, item_embedding, 0.0)
        score_after_training = self.preference_learner.predict(preference_embedding, item_embedding)
        self.assertGreater(score_before_training[0][0], score_after_training[0][0])

    def test_single_sample_single_feature_multi_pref_train_neg_no_repeat(self):
        item_embedding = [0.85]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embedding)),
                                 self.preference_learner.create_default_preference_embedding(len(item_embedding))]
        score_before_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        preference_embeddings = self.preference_learner.train(preference_embeddings, item_embedding, 0.0)
        score_after_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        self.assertGreater(score_before_training[0][0], score_after_training[0][0])
        self.assertGreater(score_before_training[1][0], score_after_training[1][0])

    def test_single_sample_multiple_feature_train_pos_no_repeat(self):
        item_embedding = [0.85, 0.0, 1.0, 1.0, 0.1]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(item_embedding))
        score_before_training = self.preference_learner.predict(preference_embedding, item_embedding)
        preference_embedding = self.preference_learner.train(preference_embedding, item_embedding, 1.0)
        score_after_training = self.preference_learner.predict(preference_embedding, item_embedding)
        self.assertGreater(score_after_training[0][0], score_before_training[0][0])

    def test_single_sample_multiple_feature_multi_pref_train_pos_no_repeat(self):
        item_embedding = [0.85, 0.0, 1.0, 1.0, 0.1]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embedding)),
                                 self.preference_learner.create_default_preference_embedding(len(item_embedding))]
        score_before_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        preference_embeddings = self.preference_learner.train(preference_embeddings, item_embedding, 1.0)
        score_after_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        self.assertGreater(score_after_training[0][0], score_before_training[0][0])
        self.assertGreater(score_after_training[1][0], score_before_training[1][0])

    def test_multi_sample_multiple_feature_multi_pref_train_pos_no_repeat(self):
        item_embeddings = [[0.85, 1.0, 0.0, 0.0, 1.0], [0.25, 0.0, 0.0, 0.0, 1.0]]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embeddings[0])),
                                 self.preference_learner.create_default_preference_embedding(len(item_embeddings[0]))]
        score_before_training = self.preference_learner.predict(preference_embeddings, item_embeddings)
        preference_embeddings = self.preference_learner.train(preference_embeddings, item_embeddings, 1.0)
        score_after_training = self.preference_learner.predict(preference_embeddings, item_embeddings)
        self.assertGreater(score_after_training[0][0], score_before_training[0][0])
        self.assertGreater(score_after_training[1][0], score_before_training[1][0])
        self.assertGreater(score_after_training[1][1], score_before_training[1][1])

    def test_single_sample_multiple_feature_train_neg_no_repeat(self):
        item_embedding = [0.85, 0.0, 1.0, 1.0, 0.1]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(item_embedding))
        score_before_training = self.preference_learner.predict(preference_embedding, item_embedding)
        preference_embedding = self.preference_learner.train(preference_embedding, item_embedding, 0.0)
        score_after_training = self.preference_learner.predict(preference_embedding, item_embedding)
        self.assertGreater(score_before_training[0][0], score_after_training[0][0])

    def test_single_sample_multiple_feature_multi_pref_train_neg_no_repeat(self):
        item_embedding = [0.85, 0.0, 1.0, 1.0, 0.1]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embedding)),
                                 self.preference_learner.create_default_preference_embedding(len(item_embedding))]
        score_before_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        preference_embeddings = self.preference_learner.train(preference_embeddings, item_embedding, 0.0)
        score_after_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        self.assertGreater(score_before_training[0][0], score_after_training[0][0])
        self.assertGreater(score_before_training[1][0], score_after_training[1][0])

    def test_same_length_unpadded_preferences(self):
        item_embedding = [0.85, 0.0, 1.0, 1.0, 0.1]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embedding) - 1),
                                 self.preference_learner.create_default_preference_embedding(len(item_embedding) - 1)]
        score_before_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        preference_embeddings = self.preference_learner.train(preference_embeddings, item_embedding, 0.0)
        score_after_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        self.assertGreater(score_before_training[0][0], score_after_training[0][0])

    def test_different_length_unpadded_preferences(self):
        item_embedding = [0.85, 0.0, 1.0, 1.0, 0.1]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embedding) - 1),
                                 self.preference_learner.create_default_preference_embedding(len(item_embedding) - 2)]
        score_before_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        preference_embeddings = self.preference_learner.train(preference_embeddings, item_embedding, 0.0)
        score_after_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        self.assertGreater(score_before_training[0][0], score_after_training[0][0])

    def test_different_length_unpadded_preferences_multiple_repeat(self):
        item_embedding = [0.85, 0.0, 1.0, 1.0, 0.1]
        preference_embeddings = [self.preference_learner.create_default_preference_embedding(len(item_embedding) - 1),
                                 self.preference_learner.create_default_preference_embedding(len(item_embedding) - 2)]
        score_before_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        preference_embeddings = self.preference_learner.train(preference_embeddings, item_embedding, 0.0, repeat=1000)
        score_after_training = self.preference_learner.predict(preference_embeddings, item_embedding)
        self.assertGreater(score_before_training[0][0], score_after_training[0][0])

    def test_pos_neg_preference_linear_feature_change(self):
        pos_item_embedding = [0.15, 0.0, 1.0, 1.0, 0.0]
        neg_item_embedding = [0.6, 0.0, 1.0, 1.0, 0.0]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(pos_item_embedding))
        preference_embedding = self.preference_learner.train(preference_embedding, pos_item_embedding, 1.0)
        preference_embedding = self.preference_learner.train(preference_embedding, neg_item_embedding, 0.0)
        pos_score = self.preference_learner.predict(preference_embedding, pos_item_embedding)[0][0]
        neg_score = self.preference_learner.predict(preference_embedding, neg_item_embedding)[0][0]
        self.assertGreater(pos_score, neg_score)

    def test_neg_pos_preference_linear_feature_change(self):
        pos_item_embedding = [0.15, 0.0, 1.0, 1.0, 0.0]
        neg_item_embedding = [0.6, 0.0, 1.0, 1.0, 0.0]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(pos_item_embedding))
        preference_embedding = self.preference_learner.train(preference_embedding, neg_item_embedding, 0.0)
        preference_embedding = self.preference_learner.train(preference_embedding, pos_item_embedding, 1.0)
        pos_score = self.preference_learner.predict(preference_embedding, pos_item_embedding)[0][0]
        neg_score = self.preference_learner.predict(preference_embedding, neg_item_embedding)[0][0]
        self.assertGreater(pos_score, neg_score)

    def test_pos_neg_preference_category_feature_change(self):
        pos_item_embedding = [0.6, 0.0, 1.0, 1.0, 0.0]
        neg_item_embedding = [0.6, 0.0, 1.0, 1.0, 1.0]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(pos_item_embedding))
        preference_embedding = self.preference_learner.train(preference_embedding, pos_item_embedding, 1.0)
        preference_embedding = self.preference_learner.train(preference_embedding, neg_item_embedding, 0.0)
        pos_score = self.preference_learner.predict(preference_embedding, pos_item_embedding)[0][0]
        neg_score = self.preference_learner.predict(preference_embedding, neg_item_embedding)[0][0]
        self.assertGreater(pos_score, neg_score)

    def test_neg_pos_preference_category_feature_change(self):
        pos_item_embedding = [0.6, 0.0, 1.0, 1.0, 0.0]
        neg_item_embedding = [0.6, 0.0, 1.0, 1.0, 1.0]
        preference_embedding = self.preference_learner.create_default_preference_embedding(len(pos_item_embedding))
        preference_embedding = self.preference_learner.train(preference_embedding, neg_item_embedding, 0.0)
        preference_embedding = self.preference_learner.train(preference_embedding, pos_item_embedding, 1.0)
        pos_score = self.preference_learner.predict(preference_embedding, pos_item_embedding)[0][0]
        neg_score = self.preference_learner.predict(preference_embedding, neg_item_embedding)[0][0]
        self.assertGreater(pos_score, neg_score)
