import unittest

from dare.DareInterface import DareInterface
from store.StoreConstants import StoreConstants
from store.StoreContext import StoreContext
from store.StoreFactory import StoreFactory


class DareInterfaceTest(unittest.TestCase):
    config = {
        "face_similarity": "$asis",
        "height": {
            "$linear": {
                "$min": 4,
                "$max": 7
            }
        },
        "age": {
            "$category": {
                "$set": [
                    19,
                    20,
                    21,
                    22,
                    23,
                    24,
                    25,
                    26,
                    27,
                    28,
                    29,
                    30,
                    31,
                    32,
                    33
                ]
            }
        },
        "weight": {
            "$linear": {
                "$min": 40,
                "$max": 180
            }
        },
        "religion": {
            "name": {
                "$category": {
                    "$set": [
                        "Hindu",
                        "Muslim - Shia",
                        "Muslim - Sunni",
                        "Muslim - Others",
                        "Christian",
                        "Sikh",
                        "Jain - Digambar",
                        "Jain - Shwetambar",
                        "Jain - Others",
                        "Parsi",
                        "Buddhist",
                        "Jewish",
                        "Inter-Religion"
                    ]
                }
            }
        },
        "birth_day": {
            "$category": {
                "$range": [
                    0,
                    7
                ]
            }
        },
        "salary": {
            "$category": {
                "$group_range": [
                    [
                        1000,
                        2000
                    ],
                    [
                        2001,
                        3000
                    ],
                    [
                        3001,
                        4000
                    ]
                ]
            }
        },
        "occupation": {
            "$category": {
                "$set": [
                    "Manager",
                    "Supervisor",
                    "Officer",
                    "Administrative Professional",
                    "Executive",
                    "Clerk",
                    "Human Resources Professional",
                    "Agriculture & Farming Professional",
                    "Pilot",
                    "Air Hostess",
                    "Airline Professional",
                    "Architect",
                    "Interior Designer",
                    "Chartered Accountant",
                    "Company Secretary",
                    "Accounts/Finance Professional",
                    "Banking Service Professional",
                    "Auditor",
                    "Financial Accountant",
                    "Financial Analyst / Planning",
                    "Fashion Designer",
                    "Beautician",
                    "Civil Services (IAS/IPS/IRS/IES/IFS)",
                    "Army",
                    "Navy",
                    "Airforce",
                    "Professor / Lecturer",
                    "Teaching / Academician",
                    "Education Professional",
                    "Hotel / Hospitality Professional",
                    "Software Professional",
                    "Hardware Professional",
                    "Engineer - Non IT",
                    "Designer",
                    "Lawyer & Legal Professional",
                    "Law Enforcement Officer",
                    "Doctor",
                    "Health Care Professional",
                    "Paramedical Professional",
                    "Nurse",
                    "Marketing Professional",
                    "Sales Professional",
                    "Journalist",
                    "Media Professional",
                    "Entertainment Professional",
                    "Event Management Professional",
                    "Advertising / PR Professional",
                    "Designer",
                    "Mariner / Merchant Navy",
                    "Scientist / Researcher",
                    "CXO / President, Director, Chairman",
                    "Business Analyst",
                    "Consultant",
                    "Customer Care Professional",
                    "Social Worker",
                    "Sportsman",
                    "Technician",
                    "Arts & Craftsman",
                    "Librarian",
                    "Business Owner / Entrepreneur"
                ]
            }
        },
        "smoker": {
            "$category": {
                "$set": [
                ]
            }
        }
    }
    user_data = [
        {
            "name": "Naveen",
            "face_similarity": 0.865,
            "height": 6.1,
            "weight": 68,
            "age": 23,
            "religion": {
                "name": "Christian"
            },
            "salary": 2400,
            "occupation": "IT",
            "smoker": 2
        },
        {
            "name": "Jaleem",
            "face_similarity": 0.654,
            "age": 33,
            "height": 6.8,
            "weight": 64,
            "religion": {
                "name": "Muslim"
            },
            "salary": 4400,
            "occupation": "IT",
            "smoker": 1
        },
        {
            "name": "Rahman",
            "face_similarity": 0.994,
            "height": 6.9,
            "weight": 68,
            "religion": {
                "name": "Muslim"
            },
            "salary": 4400,
            "occupation": "IT",
            "smoker": 0
        },
        {
            "name": "Pradeep",
            "face_similarity": 0.194,
            "height": 5.9,
            "weight": 82,
            "religion": {
                "name": "Hindu"
            },
            "salary": 2400,
            "occupation": "Government",
            "smoker": 0
        },
        {
            "name": "Vishal",
            "face_similarity": 0.194,
            "height": 5.9,
            "weight": 82,
            "religion": {
                "name": "Hindu"
            },
            "salary": 2400,
            "occupation": "Business",
            "smoker": 0
        },
        {
            "name": "Lakshmi",
            "face_similarity": 0.194,
            "height": 5.9,
            "weight": 82,
            "religion": {
                "name": "Hindu"
            },
            "salary": 2400,
            "occupation": "Private",
            "smoker": 0
        },
        {
            "name": "Abishek",
            "face_similarity": 0.194,
            "height": 5.9,
            "weight": 82,
            "religion": {
                "name": "Jain"
            },
            "salary": 2400,
            "occupation": "NIL",
            "smoker": 0
        },
        {
            "name": "Paul",
            "face_similarity": 0.194,
            "height": 5.9,
            "weight": 82,
            "religion": {
                "name": "NIL"
            },
            "salary": 2400,
            "occupation": "Software Engineer",
            "smoker": 0
        }
    ]

    def setUp(self):
        self.key = 'abcd'
        StoreFactory.embedder_store.collection.remove({})

    def tearDown(self):
        # StoreFactory.preference_store.collection.drop()
        # StoreFactory.embedder_store.collection.drop()
        StoreFactory.preference_store.collection.remove({})
        StoreFactory.embedder_store.collection.remove({})

    def test_embedder_presence_in_store(self):
        self.assertIsNone(DareInterface.get_embedding(self.key))

    def test_embedder_creation_in_store(self):
        DareInterface.create_embedder_from_config(self.key, config=DareInterfaceTest.config)
        self.assertIsNotNone(StoreFactory.embedder_store.collection.find({StoreConstants.KEY_COL_NAME: self.key}))

    def test_embedder_deletion_in_store(self):
        DareInterface.create_embedder_from_config(self.key, config=DareInterfaceTest.config)
        DareInterface.delete_embedder(self.key)
        self.assertIsNone(DareInterface.get_embedding(self.key))

    def test_single_default_user_preference_creation_from_store_context(self):
        DareInterface.create_embedder_from_config(self.key, config=DareInterfaceTest.config)
        sc = StoreContext(self.key, user_ids=[1])
        self.assertIsNotNone(sc.get_preferences())
        self.assertEqual(len(sc.get_preferences()), 1)

    def test_user_preference_deletion(self):
        DareInterface.create_embedder_from_config(self.key, config=DareInterfaceTest.config)
        sc = StoreContext(self.key, user_ids=[1, 2])
        DareInterface.clear_preference(self.key, user_ids=[1, 2])
        self.assertEqual(len(StoreFactory.preference_store.get(sc)), 0)

    def test_multi_default_user_preference_creation_from_store_context(self):
        DareInterface.create_embedder_from_config(self.key, config=DareInterfaceTest.config)
        sc = StoreContext(self.key, user_ids=[1, 2])
        self.assertEqual(len(sc.get_preferences()), 2)

    def test_single_user_single_item_prediction_from_dare_interface(self):
        DareInterface.create_embedder_from_config(self.key, config=DareInterfaceTest.config)
        score = DareInterface.predict(self.key, user_ids=[1], items=DareInterfaceTest.user_data[0:1])
        self.assertGreaterEqual(score[0][0], 0.5)

    def test_multi_user_single_item_prediction_from_dare_interface(self):
        DareInterface.create_embedder_from_config(self.key, config=DareInterfaceTest.config)
        score = DareInterface.predict(self.key, user_ids=[1, 2], items=DareInterfaceTest.user_data[0:1])
        self.assertGreaterEqual(score[0][0], 0.5)
        self.assertGreaterEqual(score[1][0], 0.5)

    def test_multi_user_multi_item_prediction_from_dare_interface(self):
        DareInterface.create_embedder_from_config(self.key, config=DareInterfaceTest.config)
        score = DareInterface.predict(self.key, user_ids=[1, 2], items=DareInterfaceTest.user_data[0:2])
        self.assertGreaterEqual(score[0][0], 0.5)
        self.assertGreaterEqual(score[0][1], 0.5)
        self.assertGreaterEqual(score[1][0], 0.5)
        self.assertGreaterEqual(score[1][1], 0.5)

    def test_single_user_single_item_train_from_dare_interface(self):
        DareInterface.create_embedder_from_config(self.key, config=DareInterfaceTest.config)
        DareInterface.train(self.key, user_ids=[1], items=DareInterfaceTest.user_data[0:1], score=[1.0])
        sc = StoreContext(key=self.key, user_ids=[1])
        pref_set = set(sc.get_preferences()[0])
        self.assertGreater(len(pref_set), 1)

    def test_multi_user_single_item_train_from_dare_interface(self):
        DareInterface.create_embedder_from_config(self.key, config=DareInterfaceTest.config)
        DareInterface.train(self.key, user_ids=[1, 2], items=DareInterfaceTest.user_data[0:1], score=[1.0])
        sc = StoreContext(key=self.key, user_ids=[1, 2])
        pref_set_1 = set(sc.get_preferences()[0])
        pref_set_2 = set(sc.get_preferences()[1])
        self.assertGreater(len(pref_set_1), 1)
        self.assertEqual(len(pref_set_1), len(pref_set_2))
