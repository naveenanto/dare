import json

from core.ItemUtility import ItemUtility

from lib.auto_embedder import Embedder

with open('sample_template.json') as template_file:
    data = json.load(template_file)

tp = Embedder(data)
tp.create_embedding_map()

with open('sample_user.json') as user_file:
    user_data = json.load(user_file)

while True:
    print('')
    idx = input('Show similar users for?' + str([user['name'] for user in user_data]) + ': ')
    item_wrapper = ItemUtility(tp, user_data[int(idx)])
    similarity, ids = item_wrapper.get_similar_items_sorted(other_items=[tp.embed(data) for data in user_data])

    print('Chosen user:', user_data[int(idx)]['name'])
    for idx in ids:
            print(similarity[idx], '->', user_data[idx])
