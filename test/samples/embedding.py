import json
import pickle

from lib.auto_embedder import Embedder

with open('sample_template.json') as template_file:
    data = json.load(template_file)

jp = Embedder(data)
jp.create_embedding_map()

jp.add_to_embedding_map({"smoker": {"$category": {"$range": [0, 2]}}})

with open('sample_user.json') as user_file:
    user_data = json.load(user_file)

for user in user_data:
    # print(user)
    emb = jp.embed(user)
    print(len(emb), emb)

print(jp.embedding_mapper['occupation'].get_inverse_map())

jp.delete_key_from_embedding_map("religion")

for user in user_data:
    # print(user)
    emb = jp.embed(user)
    print(len(emb), emb)

with open('embedding_map.emb', 'wb') as embedding_file:
    pickle.dump(jp, embedding_file)

print(jp.embedding_mapper.keys())
print(jp.embedding_mapper['occupation'].get_keys())
print(jp.embedding_mapper['occupation'].get_inverse_map())
