import json

from lib.auto_embedder import Embedder
from store.EmbedderStore import EmbedderStore

private_key = 'abcdef'

with open('sample_template.json') as template_file:
    data = json.load(template_file)

tp = Embedder(data)

estore = EmbedderStore()
# estore.insert(private_key, tp)

stored_tp = estore.load(private_key)
print(stored_tp.embedding_mapper.keys())
print(stored_tp.config)
