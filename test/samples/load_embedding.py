import json
import pickle

with open('embedding_map.emb', 'rb') as emb:
    jp = pickle.load(emb)

with open('sample_user.json') as user_file:
    user_data = json.load(user_file)

for user in user_data:
    # print(user)
    print(jp.embed(user))
