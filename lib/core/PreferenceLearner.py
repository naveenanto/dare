import numpy as np

from lib.core.ArrayUtil import ArrayUtil
from lib.core.MathLab import MathLab


class PreferenceLearner:
    PREFERNCE_DEFAULT_VALUE = 0.001

    def __init__(self, preference_default_value=PREFERNCE_DEFAULT_VALUE, activation='sigmoid'):
        self.preference_default_value = preference_default_value
        self.activation = activation

    def create_default_preference_embedding(self, length_of_embedding):
        return np.array([self.preference_default_value] * length_of_embedding)

    def pre_process(self, preference_embeddings, item_embeddings):
        item_embeddings = ArrayUtil.join_as_same_length_array(item_embeddings, pad_default_value=0.0)
        item_embeddings = ArrayUtil.convert_vector_to_matrix(item_embeddings)
        preference_embeddings = ArrayUtil.pad_uneven_length_arrays(preference_embeddings, len(item_embeddings[0]),
                                                                   pad_default_value=self.preference_default_value)
        preference_embeddings = ArrayUtil.convert_vector_to_matrix(preference_embeddings)
        return preference_embeddings, item_embeddings

    def predict(self, preference_embeddings, item_embeddings):
        preference_embeddings, item_embeddings = self.pre_process(preference_embeddings, item_embeddings)
        if self.activation == 'sigmoid':
            return MathLab.sigmoid(
                MathLab.get_matrix_multiplication_same_shape(preference_embeddings, item_embeddings))

    def update_preference(self, preference_embedding, item_embedding, score_change_rate, error, learn_rate):
        error_change_matrix = np.array(error * score_change_rate)
        identity_item_embedding = item_embedding.copy()
        identity_item_embedding[item_embedding >= 1] = 1 * learn_rate
        identity_item_embedding[item_embedding <= -1] = -1 * learn_rate
        identity_item_embedding[item_embedding == 0.0] = -learn_rate / 10
        identity_item_embedding = ArrayUtil.convert_vector_to_matrix(identity_item_embedding)
        updation_matrix = error_change_matrix.dot(identity_item_embedding)
        preference_embedding = preference_embedding + updation_matrix

        return preference_embedding

    def train(self, preference_embeddings, item_embeddings, actual_score, repeat=1):
        actual_score = ArrayUtil.convert_vector_to_matrix([actual_score])
        preference_embeddings, item_embeddings = self.pre_process(preference_embeddings, item_embeddings)
        for iter in range(repeat):
            predicted_score = self.predict(preference_embeddings, item_embeddings)
            error = actual_score - predicted_score

            score_change_rate = MathLab.derivative_sigmoid(predicted_score)
            preference_embeddings = self.update_preference(preference_embeddings, item_embeddings,
                                                           score_change_rate, error, learn_rate=0.5)
        return preference_embeddings
