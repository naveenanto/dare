import numpy as np

from lib.core.MathLab import MathLab


class ItemUtility:
    def __init__(self, embedder, item=None, embedding=None):
        """ item (or) item_embedding is required"""
        self.embedder = embedder
        if embedding is not None:
            self.embedding = self.embedder.get_updated_embedding(np.array(embedding))
        else:
            self.embedding = self.embedder.embed(item)

    def get_embedding(self):
        return self.embedder.get_updated_embedding(np.array(self.embedding))

    def get_similarity(self, other_items):
        """Get embeddings from each items and runs cosine similarity on them"""
        unpadded_embeddings = [self.get_item_mapper(item) for item in other_items]
        embeddings = [unpadded.get_embedding() for unpadded in unpadded_embeddings]
        return MathLab.cosine_similarity(self.get_embedding(), np.array(embeddings))

    def get_similar_items_sorted(self, other_items, limit=None):
        """Returns scores and sorted indices of the array"""
        similarity_scores = self.get_similarity(other_items)
        if limit is not None:
            return similarity_scores[0], np.argsort(similarity_scores[0])[::-1][:limit]
        return similarity_scores[0], np.argsort(similarity_scores[0])[::-1]

    def get_item_mapper(self, other_item=None):
        if isinstance(other_item, ItemUtility):
            other_embedding = other_item
        elif isinstance(other_item, dict):
            other_embedding = ItemUtility(self.embedder, item=other_item)
        else:
            other_embedding = ItemUtility(self.embedder, embedding=other_item)

        return other_embedding
