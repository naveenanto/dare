import numpy as np


class ArrayUtil:
    @staticmethod
    def convert_vector_to_matrix(matrix_1):
        matrix_1 = np.array(matrix_1)
        if len(matrix_1.shape) < 2:
            if matrix_1.shape[0] == 1:
                # If only one feature
                matrix_1 = matrix_1.reshape(-1, 1)
            else:
                # If only one sample
                matrix_1 = matrix_1.reshape(1, -1)

        return matrix_1

    @staticmethod
    def pad_uneven_length_arrays(arr, length, pad_default_value=0.0):
        arr = ArrayUtil.join_as_same_length_array(arr, pad_default_value)
        return ArrayUtil.pad_appropriate(arr, length, pad_default_value)

    @staticmethod
    def pad_appropriate(arr, length, pad_default_value=0.0):
        mat = ArrayUtil.convert_vector_to_matrix(arr)
        if mat.shape[1] < length:
            b = np.full((mat.shape[0], length), pad_default_value)
            b[:, :-(length - mat.shape[1])] = mat
            return b

        return mat

    @staticmethod
    def join_as_same_length_array(args, pad_default_value=0.0):
        args = np.array(args)
        if not isinstance(args[0], np.ndarray):
            return args
        max_length = 0
        for arg in args:
            max_length = max(max_length, len(arg))

        pref_embeddings = []
        for arg in args:
            pref_embeddings.append(ArrayUtil.pad_appropriate(arg, max_length, pad_default_value)[0])

        return np.array(pref_embeddings)
