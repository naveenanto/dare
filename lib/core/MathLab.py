import numpy as np
from sklearn.metrics.pairwise import cosine_similarity

from lib.core.ArrayUtil import ArrayUtil


class MathLab:
    @staticmethod
    def sigmoid(x):
        return 1 / (1 + np.exp(-x))

    @staticmethod
    def derivative_sigmoid(x):
        return x * (1 - x)

    @staticmethod
    def cosine_similarity(matrix_1, matrix_2):
        matrix_1 = ArrayUtil.convert_vector_to_matrix(matrix_1)
        matrix_2 = ArrayUtil.convert_vector_to_matrix(matrix_2)

        return cosine_similarity(matrix_1, matrix_2)

    @staticmethod
    def get_matrix_multiplication_same_shape(matrix_1, matrix_2):
        return np.array(matrix_1).dot(np.array(matrix_2).T)
