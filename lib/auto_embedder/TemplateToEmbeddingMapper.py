from lib.auto_embedder.AsIsHandler import AsIsHandler
from lib.auto_embedder.CategoryHandler import CategoryHandler
from lib.auto_embedder.LinearHandler import LinearHandler


class TemplateToEmbeddingMapper:
    # def get_index(self):
    #     return self.embedding_length
    #
    # def get_index_val(self, val):
    #     return self.template_embedding_map[val], 1.0

    @staticmethod
    def handle_category(data, start_index):
        return CategoryHandler(data, start_index)

    @staticmethod
    def handle_linear(data, start_index):
        return LinearHandler(data, start_index)

    @staticmethod
    def handle_asis(start_index):
        return AsIsHandler(start_index)

    @staticmethod
    def parse(data, key, start_index):
        if '$category' == key:
            val = TemplateToEmbeddingMapper.handle_category(data[key], start_index)
        elif '$linear' == key:
            val = TemplateToEmbeddingMapper.handle_linear(data[key], start_index)
        else:
            val = TemplateToEmbeddingMapper.handle_asis(start_index)

        return val, val.last_index

    @staticmethod
    def get_transfer_map(from_embedding_map, to_embedding_map, index, transfer_map):
        if isinstance(from_embedding_map, CategoryHandler):
            return from_embedding_map.get_transfer_map(to_embedding_map, index, transfer_map)
        elif isinstance(from_embedding_map, LinearHandler):
            from_embedding_map.get_transfer_map(to_embedding_map, transfer_map)
            return index
        else:
            from_embedding_map.get_transfer_map(to_embedding_map, transfer_map)
            return index
