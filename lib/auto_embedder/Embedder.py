import numpy as np

from lib.auto_embedder.TemplateToEmbeddingMapper import TemplateToEmbeddingMapper


# TODO: Rename key
class Embedder:
    def __init__(self, config=None):
        self.config = config
        self.embedding_mapper = {}
        self.last_index = 0
        self.create_embedding_map()

    def override_values(self, new_template_parser):
        self.embedding_mapper = new_template_parser.embedding_mapper
        self.config = new_template_parser.config
        self.last_index = new_template_parser.last_index

    def create_embedding_map(self):
        self.embedding_mapper = self.create_embedding_map_util(self.config, {})

    def add_to_template(self, config):
        self.add_to_config(self.config, config)
        self.add_to_embedding_map(config, self.embedding_mapper)

    def add_to_config(self, config, additional_config):
        for key in additional_config:
            if key in config:
                self.add_to_config(config[key], additional_config[key])
            else:
                config[key] = additional_config[key]

    def add_to_embedding_map(self, config, mapper):
        self.embedding_mapper = self.create_embedding_map_util(config, mapper)

    def delete_key_from_embedding_map(self, key):
        new_config = dict(self.config)
        new_config.pop(key)
        new_template_parser = Embedder(new_config)
        # new_template_parser.create_embedding_map()
        transfer_map = self.get_embeddings_transfer_map(self.embedding_mapper, new_template_parser,
                                                        new_template_parser.embedding_mapper, {})
        # print(transfer_map)
        # Transfer to all users
        self.override_values(new_template_parser)
        return transfer_map

    def create_embedding_map_util(self, config, mapper, parent=None):
        if type(config) is dict:
            for key in config:
                if str(key).startswith('$'):
                    mapper, self.last_index = TemplateToEmbeddingMapper.parse(config, key,
                                                                              self.last_index)
                elif type(config[key]) is str and config[key].startswith('$'):
                    mapper[key], self.last_index = TemplateToEmbeddingMapper.parse(config, key,
                                                                                   self.last_index)
                else:
                    if key in mapper:
                        mapper[key] = self.create_embedding_map_util(config[key], mapper[key])
                    else:
                        mapper[key] = self.create_embedding_map_util(config[key], {})

        return mapper

    def get_updated_embedding(self, embedding):
        """Pad embedding with remaining 0's"""
        remaining = self.last_index - len(embedding)
        if remaining > 0:
            patch = np.array([0.0] * remaining)
            embedding = np.append(embedding, patch)

        return embedding

    def embed(self, data):
        emb = [0.0] * self.last_index
        updated_emb = self.embed_util(data, self.embedding_mapper, emb,
                                      max_index=self.last_index)
        self.last_index = len(updated_emb)
        return updated_emb

    def embed_util(self, data, embedding_map, emb, max_index=0):
        for key in embedding_map:
            if key not in data or data[key] is None:
                continue
            if type(data[key]) is dict:
                emb = self.embed_util(data[key], embedding_map[key], emb, len(emb))
            else:
                value_list = data[key]
                if not isinstance(data[key], list):
                    value_list = [data[key]]
                for data_val in value_list:
                    index, value, max_index = embedding_map[key].get_index_val(data_val, max_index=len(emb))
                    addition = max_index - self.last_index
                    if addition > 0:
                        arr = np.array([0.0] * addition)
                        emb = np.append(emb, arr)
                        emb[index] = float(value)
                        self.last_index = max_index
                    else:
                        emb[index] = float(value)

        return emb

    def get_embeddings_transfer_map(self, from_embedding_mapper, template_parser, to_embedding_mapper, transfer_map):
        for key in to_embedding_mapper:
            if type(to_embedding_mapper[key]) is dict:
                self.get_embeddings_transfer_map(from_embedding_mapper[key],
                                                 template_parser, to_embedding_mapper[key], transfer_map)
            else:
                template_parser.last_index = TemplateToEmbeddingMapper.get_transfer_map(
                    from_embedding_mapper[key],
                    to_embedding_mapper[key],
                    template_parser.last_index, transfer_map)

        return transfer_map
