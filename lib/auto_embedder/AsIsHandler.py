class AsIsHandler:
    def __init__(self, start_index):
        self.index = start_index
        self.last_index = start_index + 1

    def get_index_val(self, val, max_index=0):
        return self.index, val, max_index

    def get_transfer_map(self, to_embedding_map, transfer_map):
        transfer_map[self.index] = to_embedding_map.index
