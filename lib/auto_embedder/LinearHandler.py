class LinearHandler:
    def __init__(self, data, start_index):
        self.data = data
        self.index = start_index
        self.last_index = start_index
        self.min_max = None
        self.mean_std = None
        self.parse(data)

    def get_index_val(self, val, max_index=0):
        if self.min_max:
            return self.index, "{:6f}".format(
                ((val - self.min_max[0]) / (self.min_max[1] - self.min_max[0])) - 0.5), max_index
        elif self.mean_std:
            return self.index, "{:6f}".format((val - self.mean_std[0]) / self.mean_std[1]), max_index

    def parse(self, data):
        if '$min' in data and '$max' in data:
            self.last_index += 1
            self.min_max = [data['$min'], data['$max']]
        elif '$mean' in data and '$std' in data:
            self.last_index += 1
            self.mean_std = [data['$mean'], data['$std']]

    def get_transfer_map(self, to_embedding_map, transfer_map):
        transfer_map[self.index] = to_embedding_map.index
        # return transfer_map
