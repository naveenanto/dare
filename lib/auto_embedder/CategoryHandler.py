class CategoryHandler:
    def __init__(self, data, start_index):
        self.data = data
        self.index = start_index
        self.last_index = start_index
        self.group_range = False
        self.index_map = {}
        self.parse(data)

    def get_index_val(self, val, max_index=0):
        if val in self.index_map:
            return self.index_map[val], 1, max_index
        elif self.group_range:
            for start, end in self.index_map:
                if not isinstance(start, str) and start <= val <= end:
                    return self.index_map[(start, end)], 1, max_index
            return self.index_map[('start', 'end')], 1, max_index
        else:
            self.index_map[val] = max_index
            return self.index_map[val], 1, max_index + 1

    def get_keys(self):
        return self.index_map.keys()

    def get_inverse_map(self):
        imap = {}
        for key in self.index_map.keys():
            imap[self.index_map[key]] = key
        return imap

    def parse(self, data):
        if '$set' in data:
            collection = set(data['$set'])
            for elem in collection:
                self.index_map[elem] = self.last_index
                self.last_index += 1

        elif '$range' in data:
            range_data = data['$range']
            range_val = range_data[1] - range_data[0] + 1
            for i in range(range_val):
                self.index_map[range_data[0] + i] = self.last_index
                self.last_index += 1
        elif '$group_range' in data:
            self.group_range = True
            for range_val in data['$group_range']:
                self.index_map[(range_val[0], range_val[1])] = self.last_index
                self.last_index += 1
            # Extra group end
            self.index_map[('start', 'end')] = self.last_index
            self.last_index += 1

    def get_transfer_map(self, to_embedding_map, index, transfer_map):
        for elem in self.index_map:
            if elem in to_embedding_map.index_map:
                transfer_map[self.index_map[elem]] = to_embedding_map.index_map[elem]
            else:
                transfer_map[self.index_map[elem]] = index + 1
                index = index + 1

        return index
