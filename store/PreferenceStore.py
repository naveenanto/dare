from store import db
from store.StoreConstants import StoreConstants


# TODO: Make it Singleton
class PreferenceStore:
    PRECISION = 6

    def __init__(self, database=db, store_name=None):
        if store_name is None:
            store_name = StoreConstants.PREFERENCE_STORE_NAME
        self.collection = database[store_name]

    @staticmethod
    def encode(preference_embedding):
        return [int(item * pow(10, PreferenceStore.PRECISION)) for item in preference_embedding]

    @staticmethod
    def decode(preference_embedding):
        return [float(item / pow(10, PreferenceStore.PRECISION)) for item in preference_embedding]

    def insert(self, store_context, preference_embedding, user_id):
        params = store_context.get_params_for_preference_store_without_user()
        params[StoreConstants.USER_ID_COL_NAME] = user_id
        params[StoreConstants.PREFERENCE_COL_NAME] = PreferenceStore.encode(preference_embedding)
        return self.collection.insert_one(params).inserted_id

    def delete(self, store_context):
        self.collection.remove(store_context.get_params_for_preferences_store())

    def get_by_id(self, _id):
        item = self.collection.find_one({'_id': _id})
        if item is not None:
            item[StoreConstants.PREFERENCE_COL_NAME] = PreferenceStore.decode(item[StoreConstants.PREFERENCE_COL_NAME])
        return item

    def update(self, id_val, preference_embedding):
        return self.collection.update_one({
            '_id': id_val
        }, {
            '$set': {
                StoreConstants.PREFERENCE_COL_NAME: PreferenceStore.encode(preference_embedding)
            }
        }, upsert=False)

    def get(self, store_context):
        items_cursor = self.collection.find(store_context.get_params_for_preferences_store())
        items = [item for item in items_cursor]
        if items is not None:
            for item in items:
                item[StoreConstants.PREFERENCE_COL_NAME] = PreferenceStore.decode(
                    item[StoreConstants.PREFERENCE_COL_NAME])

        return items
