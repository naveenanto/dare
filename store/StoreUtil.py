from bson import BSON

from store.StoreConstants import StoreConstants
from store.StoreFactory import StoreFactory


class StoreUtil:
    @staticmethod
    def stats(collection, key=None, conv=1):
        if key is not None:
            find_all_cursor = collection.find({StoreConstants.KEY_COL_NAME: key})
        else:
            find_all_cursor = collection.find()
        sizes = [len(BSON.encode(doc)) / conv for doc in find_all_cursor]
        total_size = sum(sizes)
        if len(sizes) != 0:
            avg_size = total_size / len(sizes)
        else:
            avg_size = 0.0
        return {'count': len(sizes), 'total_size': total_size, 'avg_size': avg_size}

    @staticmethod
    def get_db_stats(token, conv=1):
        e_store = StoreUtil.stats(collection=StoreFactory.embedder_store.collection, key=token, conv=conv)
        p_store = StoreUtil.stats(collection=StoreFactory.preference_store.collection, key=token, conv=conv)
        return e_store, p_store
