import numpy as np

from lib.core.PreferenceLearner import PreferenceLearner
from store.StoreConstants import StoreConstants
from store.StoreFactory import StoreFactory


class StoreContext:
    def __init__(self, key, class_name='default', user_ids=None, embedder=None, preferences=None,
                 lazy_initialization=False):
        self.key = key
        self.class_name = class_name
        self.user_ids = user_ids
        self.stores = StoreContext.initialize_stores()
        self.objects = {}
        if not lazy_initialization:
            self.objects = self.initialize_objects(embedder, preferences)

    def get_params_for_embedder_store(self):
        return {StoreConstants.KEY_COL_NAME: self.key, StoreConstants.CLASS_NAME_COL_NAME: self.class_name}

    def get_params_for_preferences_store(self):
        return {StoreConstants.KEY_COL_NAME: self.key, StoreConstants.CLASS_NAME_COL_NAME: self.class_name,
                StoreConstants.USER_ID_COL_NAME: {"$in": self.user_ids}}

    def get_params_for_preference_store_without_user(self):
        return {StoreConstants.KEY_COL_NAME: self.key, StoreConstants.CLASS_NAME_COL_NAME: self.class_name}

    def get_embedder_obj(self):
        if 'embedder_db_obj' in self.objects:
            return self.objects['embedder_db_obj']

        e_store = self.stores['e_store']
        embedder = e_store.get(self)
        return embedder

    def get_embedder(self):
        embedder_obj = self.get_embedder_obj()
        return embedder_obj[StoreConstants.EMBEDDER_COL_NAME]

    def get_preference_obj(self, length_of_embedding=None):
        if 'preference_db_obj' in self.objects:
            return self.objects['preference_db_obj']

        p_store = self.stores['p_store']
        preferences = p_store.get(self)
        if len(preferences) < len(self.user_ids):
            existing_ids = set([pref[StoreConstants.USER_ID_COL_NAME] for pref in preferences])
            for user_id in self.user_ids:
                if user_id not in existing_ids:
                    insert_id = p_store.insert(self, [PreferenceLearner.PREFERNCE_DEFAULT_VALUE] * length_of_embedding,
                                               user_id)
                    preferences.append(p_store.get_by_id(insert_id))
        return preferences

    def get_preferences(self):
        preference_obj = self.get_preference_obj(length_of_embedding=self.get_embedder().last_index)
        return [np.array(item[StoreConstants.PREFERENCE_COL_NAME]) for item in preference_obj]

    def update_embedder(self, embedder):
        e_store = self.stores['e_store']
        e_store.update(self.objects['embedder_db_obj']['_id'], embedder)
        self.objects['embedder_db_obj'][StoreConstants.EMBEDDER_COL_NAME] = embedder

    def update_preferences(self, preferences):
        for i in range(len(preferences)):
            _id = self.objects['preference_db_obj'][i]['_id']
            pref = preferences[i]
            self.update_preference(_id, pref)
            self.objects['preference_db_obj'][i][StoreConstants.PREFERENCE_COL_NAME] = pref

    def update_preference(self, pref_id, preference):
        p_store = self.stores['p_store']
        p_store.update(pref_id, preference)
        # self.objects['preference_db_obj'] = update.raw_result

    @staticmethod
    def initialize_stores():
        return {
            'e_store': StoreFactory.embedder_store,
            'p_store': StoreFactory.preference_store
        }

    def initialize_objects(self, embedder, preferences):
        objects = {}
        if embedder is None:
            objects['embedder_db_obj'] = self.get_embedder_obj()
        else:
            objects['embedder_db_obj'] = embedder

        if preferences is None and self.user_ids is not None:
            objects['preference_db_obj'] = self.get_preference_obj(objects['embedder_db_obj']['embedder'].last_index)
        else:
            objects['preference_db_obj'] = preferences

        return objects
