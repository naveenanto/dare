import pickle

from bson.binary import Binary
from pymongo import ASCENDING

from lib.auto_embedder.Embedder import Embedder
from store import db
from store.StoreConstants import StoreConstants


# TODO: Make it Singleton
class EmbedderStore:
    def __init__(self, database=db, store_name=None):
        if store_name is None:
            store_name = StoreConstants.EMBEDDER_STORE_NAME
        self.collection = database[store_name]
        self.collection.create_index(
            [(StoreConstants.TOKEN_COL_NAME, ASCENDING), (StoreConstants.CLASS_NAME_COL_NAME, ASCENDING)], unique=True)

    def insert(self, store_context, embedder):
        if isinstance(embedder, Embedder):
            embedder_binary = pickle.dumps(embedder)
            params = store_context.get_params_for_embedder_store()
            params[StoreConstants.EMBEDDER_COL_NAME] = Binary(embedder_binary)
            self.collection.insert_one(params)

    def delete(self, store_context):
        self.collection.remove(store_context.get_params_for_embedder_store())

    def update(self, id_val, embedder):
        if isinstance(embedder, Embedder):
            embedder_binary = Binary(pickle.dumps(embedder))
        # else:
        #     embedder_binary = embedder
        return self.collection.update_one({
            '_id': id_val
        }, {
            '$set': {
                StoreConstants.EMBEDDER_COL_NAME: embedder_binary
            }
        }, upsert=False)

    def get(self, store_context):
        item = self.collection.find_one(store_context.get_params_for_embedder_store())
        if item is not None:
            item[StoreConstants.EMBEDDER_COL_NAME] = pickle.loads(item[StoreConstants.EMBEDDER_COL_NAME])

        return item
