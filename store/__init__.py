import os

from pymongo import MongoClient

MONGO_URL = os.environ.get('MONGODB_URI')
if not MONGO_URL:
    MONGO_URL = "mongodb://localhost:27017/store"

mongo = MongoClient(MONGO_URL)

db = mongo.get_database()
