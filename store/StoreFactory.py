from store.EmbedderStore import EmbedderStore
from store.PreferenceStore import PreferenceStore
from store.TokenStore import TokenStore

class StoreFactory:
    embedder_store = EmbedderStore()
    preference_store = PreferenceStore()
    token_store = TokenStore()
