from store import db
from store.StoreConstants import StoreConstants


# TODO: Make it Singleton
class TokenStore:
    def __init__(self, database=db, store_name=None):
        if store_name is None:
            store_name = StoreConstants.TOKEN_STORE_NAME
        self.collection = database[store_name]
        self.collection.create_index(StoreConstants.TOKEN_COL_NAME, unique=True)

    def insert(self, data):
        self.collection.insert_one(data)

    def get(self, token):
        return self.collection.find_one({StoreConstants.TOKEN_COL_NAME: token})
