sample_embedding = {
    "face_similarity": "$asis",
    "height": {
        "$linear": {
            "$min": 4,
            "$max": 7
        }
    },
    "age": {
        "$category": {
            "$set": [
            ]
        }
    },
    "weight": {
        "$linear": {
            "$min": 40,
            "$max": 180
        }
    },
    "religion": {
        "name": {
            "$category": {
                "$set": [
                ]
            }
        }
    },
    "birth_day": {
        "$category": {
            "$range": [
                0,
                7
            ]
        }
    },
    "salary": {
        "$category": {
            "$group_range": [
                [
                    1000,
                    2000
                ],
                [
                    2001,
                    3000
                ],
                [
                    3001,
                    4000
                ]
            ]
        }
    },
    "occupation": {
        "$category": {
            "$set": [
            ]
        }
    },
    "smoker": {
        "$category": {
            "$set": [
            ]
        }
    }
}

sample_data = {
    "name": "Paul",
    "age": 23,
    "birth_day": 5,
    "face_similarity": 0.194,
    "height": 5.9,
    "weight": 82,
    "religion": {
        "name": "NIL"
    },
    "salary": 2400,
    "occupation": "Software Engineer",
    "smoker": 0
}

sample_db = {
    "name": "String",
    "age": "Integer",
    "face_similarity": "Float",
    "height": "Float",
    "weight": "Float",
    "religion": {
        "name": "String",
        "sub_religion": "String"
    },
    "salary": "Float",
    "occupation": "String",
    "smoker": "enum",
    "birth_day": "Integer"
}

sample_pred_input = {
    "data": {
        "user_ids": ["user_id_1", "user_id_2"], "item_data": [{
            "name": "Paul",
            "face_similarity": 0.194,
            "height": 5.9,
            "weight": 82,
            "religion": {
                "name": "MOMO"
            },
            "salary": 2400,
            "occupation": "Software Engineer",
            "smoker": 0
        }, {
            "name": "Naveen",
            "face_similarity": 0.865,
            "height": 6.1,
            "weight": 68,
            "age": 23,
            "religion": {
                "name": "Christian"
            },
            "salary": 2400,
            "occupation": "IT",
            "smoker": 2
        }]
    }
}

sample_pred_output = {
    "score": [
        [
            0.4787063037125234,
            0.49568929555776975
        ],
        [
            0.6259762501041548,
            0.5342714115651329
        ]
    ]
}

sample_train_input = {
    "data": {
        "user_ids": ["user_id_1"], "item_data": [{
            "name": "Paul",
            "face_similarity": 0.194,
            "height": 5.9,
            "weight": 82,
            "religion": {
                "name": "MOMO"
            },
            "salary": 2400,
            "occupation": "Software Engineer",
            "smoker": 0
        }], "scores": [0.99]
    }
}

sample_train_output = {
    "train": true
}

sample_embedding_api_input = {
    "data": {
        "type": "create",
        "template": {
            "face_similarity": "$asis",
            "height": {
                "$linear": {
                    "$min": 4,
                    "$max": 7
                }
            },
            "age": {
                "$category": {
                    "$set": [
                    ]
                }
            },
            "religion": {
                "name": {
                    "$category": {
                        "$set": [
                        ]
                    }
                }
            },
            "birth_day": {
                "$category": {
                    "$range": [
                        0,
                        7
                    ]
                }
            },
            "salary": {
                "$category": {
                    "$group_range": [
                        [
                            1000,
                            2000
                        ],
                        [
                            2001,
                            3000
                        ],
                        [
                            3001,
                            4000
                        ]
                    ]
                }
            }
        }
    }
}