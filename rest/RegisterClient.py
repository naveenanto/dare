import datetime
import json
import os
import secrets

import jwt
import requests
from flask import request
from flask_restful import Resource, reqparse

from rest import api
from rest.constants.ApiConstants import ApiConstants
from store.StoreFactory import StoreFactory


class RegisterToken(Resource):
    def __init__(self, *args, **kwargs):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_info', type=str)
        self.parser.add_argument('token_info', type=str)
        self.parser.add_argument('details', type=dict)
        super(RegisterToken, self).__init__()

    def get(self):
        requirements = {'expiration': {"type": "number", "min": 30, "max": 360,
                                       "description": "Number of days till expirations",
                                       "costpv": 3.33},
                        'max_user_count': {"type": "number", "min": 1000, "max": 10000,
                                           "description": "Maximum number of users",
                                           "costpv": 0.6},
                        'max_size': {"type": "number", "min": 1024, "max": 10240,
                                     "description": "Maximum DB storage in Kbs",
                                     "costpv": 0.05}}
        final_map = {'name': 'DARE', 'requirements': requirements, 'base_url': request.host_url,
                     'short_dec': 'Dynamic Adaptable Recommendation Engine',
                     'long_desc': 'Build your custom recommendation engine in 30 mins',
                     'img': 'https://900913.ru/media/tag/configure_n2qKAD3.jpg'}
        response = requests.post(os.getenv('TOKENIZER_URL') + '/register_app', data={'data': json.dumps(final_map)})
        # print(response.content)
        return response.json()

    def post(self):
        vals = self.parser.parse_args()
        try:
            decoded = jwt.decode(vals['token_info'], os.getenv('jwt_key'), algorithms=['HS256'])
            # print(decoded)
            decoded['enabled'] = True
            decoded['expiration'] = datetime.datetime.utcnow() + datetime.timedelta(days=int(decoded['expiration']))
            decoded['created_at'] = datetime.datetime.utcnow()
            decoded['hit_count'] = 0
            decoded['token'] = secrets.token_urlsafe(16)
            private_key, public_key = RegisterToken.generate_private_public_key(decoded)
            decoded['private_key'] = private_key
            decoded['public_key'] = public_key
            StoreFactory.token_store.insert(decoded)
            return decoded
        except Exception as e:
            print(e)
            return {"error": str(e)}

    @staticmethod
    def generate_private_public_key(decoded):
        private_decoded = {'token': decoded['token'], 'scope': ApiConstants.PRIVATE_SCOPE}
        private_decoded['key'] = jwt.encode(private_decoded, os.getenv('jwt_key')).decode('utf-8')
        public_decoded = {'token': decoded['token'], 'scope': ApiConstants.PUBLIC_SCOPE}
        public_decoded['key'] = jwt.encode(public_decoded, os.getenv('jwt_key')).decode('utf-8')
        return private_decoded['key'], public_decoded['key']


api.add_resource(RegisterToken, '/register_token')
