import os

import jwt
from flask import request, g

from rest import app
from rest import output_json
from rest.constants.ApiConstants import ApiConstants
from rest.handlers.TokenHandler import TokenHandler


def get_token_from_request():
    token = request.headers.get(ApiConstants.TOKEN_HEADER_KEY_NAME)
    if token is None:
        token = request.args.get(ApiConstants.TOKEN_HEADER_KEY_NAME)

    return token


@app.before_request
def before_request():
    try:
        if not request.method == 'OPTIONS' and request.endpoint and 'api' in request.base_url:
            token = get_token_from_request()
            if not token:
                return output_json({"error": "Missing validation token"}, 400)
            token_info = jwt.decode(token, os.getenv('jwt_key'), algorithms=['HS256'])
            access_token = token_info['token']
            scope = token_info['scope']
            g.token = access_token
            g.scope = scope
            g.token_handler = TokenHandler(access_token)
            message, valid = g.token_handler.is_valid()
            if not valid:
                return output_json({"error": message}, 400)
    except Exception as e:
        return output_json({"error": str(e)}, 400)
