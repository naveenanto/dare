import numpy as np
from flask import g
from flask_restful import Resource, reqparse

from dare.DareInterface import DareInterface
from rest import api
from rest.constants.ApiConstants import ApiConstants


class Embedding(Resource):
    def __init__(self, *args, **kwargs):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('data', type=dict)
        super(Embedding, self).__init__()

    def post(self):
        if g.scope != ApiConstants.PRIVATE_SCOPE:
            return {'error': 'Invalid scope access'}

        vals = self.parser.parse_args()['data']
        token = g.token
        if vals['type'] == 'create':
            DareInterface.create_embedder_from_config(key=token, config=vals['template'])
        elif vals['type'] == 'add':
            DareInterface.add_to_embedding(key=token, config=vals['template'])

        return {'Creation': True}


class Predict(Resource):
    def __init__(self, *args, **kwargs):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('data', type=dict)
        super(Predict, self).__init__()

    def post(self):
        vals = self.parser.parse_args()['data']
        token = g.token
        score = DareInterface.predict(token, user_ids=vals['user_ids'], items=vals['item_data'])
        return {'score': score}


class Train(Resource):
    def __init__(self, *args, **kwargs):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('data', type=dict)
        super(Train, self).__init__()

    def post(self):
        vals = self.parser.parse_args()['data']
        token = g.token
        DareInterface.train(token, user_ids=vals['user_ids'], items=vals['item_data'], score=vals['scores'])
        return {'train': True}


class Preference(Resource):
    def __init__(self, *args, **kwargs):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('userids')
        super(Preference, self).__init__()

    def delete(self):
        vals = self.parser.parse_args()
        token = g.token
        user_ids = vals['userids']
        if not isinstance(user_ids, list):
            user_ids = [user_ids]

        try:
            DareInterface.clear_preference(token, user_ids=user_ids)
            return {"status": True}
        except Exception as e:
            return {"error": str(e)}

    def post(self):
        vals = self.parser.parse_args()
        token = g.token
        user_ids = vals['userids']
        if not isinstance(user_ids, list):
            user_ids = [user_ids]

        preference, index_map = DareInterface.get_preference(token, user_ids)
        pref_np = np.array(preference[0])
        max_v = np.max(pref_np)
        min_v = np.min(pref_np) - 0.001
        pref_map = {}
        for key in index_map:
            if int(key) < len(pref_np):
                pref_map[str(index_map[key])] = round(((pref_np[int(key)] - min_v) / (max_v - min_v)) * 100, 1)

        return pref_map


api.add_resource(Predict, '/api/predict')
api.add_resource(Train, '/api/train')
api.add_resource(Embedding, '/api/embedding')
api.add_resource(Preference, '/api/preference')
