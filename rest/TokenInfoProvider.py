from flask import g
from flask_restful import Resource, reqparse

from rest import api


class TokenInfoProvider(Resource):
    def __init__(self, *args, **kwargs):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_info', type=str)
        self.parser.add_argument('details', type=dict)
        super(TokenInfoProvider, self).__init__()

    def get(self):
        # vals = self.parser.parse_args()
        try:
            th = g.token_handler
            return th.stats()
        except Exception as e:
            return {"error": str(e)}

    def delete(self):
        vals = self.parser.parse_args()
        try:
            th = g.token_handler
            try:
                th.remove()
            except Exception as e:
                return {'status': False}

            return {'status': True}
        except Exception as e:
            return {"error": str(e)}


api.add_resource(TokenInfoProvider, '/token')
