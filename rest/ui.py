import json
import os
from functools import wraps

import jwt
from flask import session, redirect, render_template, request, g

import rest.constants.UIConstants as constants
from dare.DareInterface import DareInterface
from rest import app, output_json
from rest.handlers.TokenHandler import TokenHandler
from store.StoreConstants import StoreConstants


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if constants.PROFILE_KEY not in session:
            return redirect('/login')
        # print(session[constants.PROFILE_KEY])
        return f(*args, **kwargs)

    return decorated


@app.route('/')
@requires_auth
def home():
    embedding = DareInterface.get_embedding(session[constants.PROFILE_KEY])
    if embedding is None:
        return redirect('/create-embedding-view')
    config_str = json.dumps(embedding[StoreConstants.EMBEDDER_COL_NAME].config)
    token_handler = TokenHandler(session[constants.PROFILE_KEY])
    return render_template('dashboard.html', config=config_str, stats=token_handler.stats())


@app.route('/logout')
def logout():
    session.clear()
    return redirect('/login')

@app.route('/create-embedding-view')
@requires_auth
def create_embedding_view():
    return render_template('create_embedding.html')


@app.route('/create-embedding-form', methods=['POST'])
@requires_auth
def create_embedding_form():
    file = request.files['file']
    if 'application/json' in file.content_type:
        DareInterface.delete_embedder(session[constants.PROFILE_KEY])
        DareInterface.create_embedder_from_config(session[constants.PROFILE_KEY], json.load(file))
        # print(json.load(file))
    return redirect('/')


@app.route('/login', methods=['GET'])
def login_view():
    return render_template('token_login.html')


@app.route('/login', methods=['POST'])
def login_action():
    token = request.form.get(constants.PROFILE_KEY)
    token_info = jwt.decode(token, os.getenv('jwt_key'), algorithms=['HS256'])
    access_token = token_info['token']
    scope = token_info['scope']
    session[constants.PROFILE_KEY] = access_token
    session['scope'] = scope
    g.token_handler = TokenHandler(access_token)
    message, valid = g.token_handler.is_valid()
    if not valid:
        return output_json({"error": message}, 400)
    # session['token_info'] = token_info
    return redirect('/')
