import datetime

from rest.constants.ApiConstants import ApiConstants
from store.StoreFactory import StoreFactory
from store.StoreUtil import StoreUtil


class TokenHandler:
    def __init__(self, token):
        self.token = token
        self.token_obj = StoreFactory.token_store.get(token)

    def is_valid(self):
        if self.exists() and self.enabled() and not self.is_expired():
            e_stats, p_stats = StoreUtil.get_db_stats(self.token, conv=1024)
            total_size = e_stats['total_size'] + p_stats['total_size']
            if total_size < self.max_size():
                if p_stats['count'] < self.max_user_count():
                    return 'valid', True
                return 'User count exceeded', False
            return 'Total size exceeded', False

        return 'User doesn\'t exists or disabled or expired', False

    def exists(self):
        return self.token_obj

    def is_expired(self):
        b = self.token_obj['expiration']
        a = datetime.datetime.now()
        return (b - a).seconds <= 0

    def days_till_expiration(self):
        b = self.token_obj['expiration']
        # b.replace(tzinfo=None)
        a = datetime.datetime.now()
        return (b - a).days

    def enabled(self):
        return self.token_obj['enabled']

    def max_size(self):
        if 'max_size' not in self.token_obj:
            return ApiConstants.DB_STORAGE_MAX_SIZE
        return float(self.token_obj['max_size'])

    def max_user_count(self):
        if 'max_user_count' not in self.token_obj:
            return ApiConstants.DB_STORAGE_MAX_USER_COUNT
        return int(self.token_obj['max_user_count'])

    def increment(self, token_store_field='hit_count', value=1):
        StoreFactory.token_store.collection.update({'_id': self.token_obj['_id']},
                                                   {'$inc': {token_store_field: value}})

    def stats(self, conv=1024, round_val=2):
        e_stats, p_stats = StoreUtil.get_db_stats(self.token, conv)
        total_size = e_stats['total_size'] + p_stats['total_size']
        remaining_size = round(self.max_size() - total_size, round_val)
        remaining_user_count = self.max_user_count() - p_stats['count']
        return {'remaining_size': remaining_size,
                'remaining_user_count': remaining_user_count,
                'embedder_stats': e_stats,
                'user_stats': p_stats}

    def remove(self):
        StoreFactory.token_store.collection.remove({'_id': self.token_obj['id']})
