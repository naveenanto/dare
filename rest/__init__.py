import logging
from os.path import join, dirname

from bson.json_util import dumps
from dotenv import load_dotenv
from flask import Flask
from flask import make_response
from flask_cors import CORS
from flask_restful import Api

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path=dotenv_path, verbose=True)

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

app = Flask(__name__)
app.secret_key = 'super secret key'
app.config['MAX_CONTENT_LENGTH'] = 1 * 1024 * 1024
cor = CORS(app, supports_credentials=True)

def output_json(obj, code, headers=None):
    resp = make_response(dumps(obj), code)
    resp.headers.extend(headers or {})
    return resp


DEFAULT_REPRESENTATIONS = {'application/json': output_json}
api = Api(app)
api.representations = DEFAULT_REPRESENTATIONS

import rest.RegisterClient
import rest.Validator
import rest.TokenInfoProvider
import rest.Interface
import rest.ui
