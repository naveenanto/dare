import numpy as np

from dare import preference_learner
from lib.auto_embedder.CategoryHandler import CategoryHandler
from lib.auto_embedder.Embedder import Embedder
from store.StoreConstants import StoreConstants
from store.StoreContext import StoreContext
from store.StoreFactory import StoreFactory


class DareInterface:
    @staticmethod
    def get_embedding(key, class_name='default'):
        store_context = StoreContext(key, class_name, lazy_initialization=True)
        embedding_obj = StoreFactory.embedder_store.get(store_context)
        return embedding_obj

    @staticmethod
    def add_to_embedding(key, config, class_name='default'):
        store_context = StoreContext(key, class_name, lazy_initialization=True)
        embedding_obj = StoreFactory.embedder_store.get(store_context)
        embedder = embedding_obj[StoreConstants.EMBEDDER_COL_NAME]
        embedder.add_to_template(config)
        StoreFactory.embedder_store.update(embedding_obj['_id'], embedder)
        return embedding_obj

    @staticmethod
    def create_embedder_from_config(key, config, class_name='default'):
        embedder = Embedder(config=config)
        store_context = StoreContext(key, class_name, embedder=embedder)
        StoreFactory.embedder_store.insert(store_context, embedder)

    @staticmethod
    def delete_embedder(key, class_name='default'):
        store_context = StoreContext(key, class_name)
        StoreFactory.embedder_store.delete(store_context)
        # StoreFactory.embedder_store.insert(store_context, embedder)

    @staticmethod
    def get_preference(key, user_ids, class_name='default'):
        store_context = StoreContext(key, class_name, user_ids=user_ids)
        embedder = store_context.get_embedder().embedding_mapper
        keys = {}
        for key in embedder:
            if isinstance(embedder[key], CategoryHandler):
                keys.update(embedder[key].get_inverse_map())

        return store_context.get_preferences(), keys

    @staticmethod
    def clear_preference(key, user_ids, class_name='default'):
        store_context = StoreContext(key, class_name, user_ids=user_ids)
        StoreFactory.preference_store.delete(store_context)

    @staticmethod
    def get_item_pref_embedding(key, user_ids, items, class_name='default'):
        store_context = StoreContext(key, class_name, user_ids=user_ids)
        embedder = store_context.get_embedder()
        item_emb = [np.array(embedder.embed(item)) for item in items]
        store_context.update_embedder(store_context.get_embedder())
        preference_emb = store_context.get_preferences()
        return item_emb, preference_emb, store_context

    @staticmethod
    def predict(key, user_ids, items, class_name='default'):
        item_emb, preference_emb, store_context = DareInterface.get_item_pref_embedding(key, user_ids, items,
                                                                                        class_name)
        score = preference_learner.predict(preference_embeddings=preference_emb,
                                           item_embeddings=item_emb)
        return score

    @staticmethod
    def train(key, user_ids, items, score, class_name='default'):
        item_emb, preference_emb, store_context = DareInterface.get_item_pref_embedding(key, user_ids, items,
                                                                                        class_name)
        new_pref_embedding = preference_learner.train(preference_embeddings=preference_emb,
                                                      item_embeddings=item_emb, actual_score=score)
        store_context.update_preferences([pref.tolist() for pref in new_pref_embedding])
