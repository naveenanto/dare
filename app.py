from os import environ as env

from rest import app, logger

logger.info('Starting app')
app.run(host='0.0.0.0', port=env.get('PORT', 5000))
